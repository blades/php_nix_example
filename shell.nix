{ sources ? import ./nix/sources.nix,
  pkgs ? import sources.nixpkgs {} }:

let
  composer2nix = ( import "${sources.composer2nix}/release.nix" {} ).package."${pkgs.system}";
in pkgs.mkShell {
  buildInputs = [
    pkgs.php
    pkgs.phpPackages.composer
    composer2nix

    # keep this line if you use bash
    pkgs.bashInteractive
  ];
}
