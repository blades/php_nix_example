{ sources ? import ./nix/sources.nix,
  pkgs ? import sources.nixpkgs {} }:

let
  composition = import ./composition.nix { inherit pkgs; };
in composition
